﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppApi.Entities
{
    public class OutGateInputDto
    {
        public string RegisterNo { get; set; }
        public int CardId { get; set; }
        public int OutGateEmpId { get; set; }
    }
}
